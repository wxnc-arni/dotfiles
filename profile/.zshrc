export LANG=en_US.UTF-8

if [ "$(sysctl -n sysctl.proc_translated)" = "1" ]; then
  local brew_path="/usr/local/homebrew/bin"
  local brew_opt_path="/usr/local/opt"
  local rbenv_path="$HOME/.rbenv-x86"
  local pyenv_path="$HOME/.pyenv-x86"
  local nvm_path="$HOME/.nvm-x86"
else
  local brew_path="/opt/homebrew/bin"
  local brew_opt_path="/opt/homebrew/opt"
  local rbenv_path="$HOME/.rbenv"
  local pyenv_path="$HOME/.pyenv"
  local nvm_path="$HOME/.nvm"
fi

export RBENV_ROOT="$rbenv_path"
export PATH="$RBENV_ROOT/bin:$PATH"
eval "$(rbenv init - zsh)"

export PYENV_ROOT="$pyenv_path" 
export PATH="$PYENV_ROOT/bin:$PATH" 
eval "$(pyenv init --path)" 
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

eval "$(nodenv init -)"

for file in ~/.zsh/*; do
  source "$file"
done